﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebTest.Models;
using System.Linq;
using System;
using WebTest.Services;

namespace WebTest.Controllers
{
    [ApiController]
    public class AnswersController : ControllerBase
    {
        private readonly WooliesClient _wooliesClient;
        private readonly IConfiguration _configuration;
        private readonly ITrolleyCalculatorService _trolleyCalculatorService;
        private readonly IProductService _productSortService; 
        private string _token;

        public AnswersController(
            IConfiguration Configuration, 
            ITrolleyCalculatorService trolleyCalculatorService, 
            IProductService productSortService)
        {
            _configuration = Configuration;
            _trolleyCalculatorService = trolleyCalculatorService;
            _productSortService = productSortService;

            _token = _configuration["Token"];
            if (_token == null)
            {
                throw new Exception("Please specify the Token in the appSettings.json");
            }

            string url = _configuration["WooliesApi"];
            if (url == null)
            {
                throw new Exception("Please specify the woolies api URL in the appSettings.json");
            }

            //TODO inject woolies client with URL
            _wooliesClient = new WooliesClient(url);
        }

        /// <summary>
        /// Retrieve the user's name and token for the woolies API.
        /// </summary>
        /// <returns>User name and token.</returns>
        [HttpGet("/user")]
        public ActionResult<User> GetUser()
        {
            // extremely simple logic, doesn't require a service layer for demo purposes.
            string name = _configuration["Name"];
            if (name == null)
            {
                return NotFound();
            }
            return new User {
                Name = name,
                Token = _token
            };
        }
        
        /// <summary>
        /// Sort woolies products
        /// </summary>
        /// <param name="sortOption">The type of sorting to perform (see SortType enum).</param>
        /// <returns>Sorted/orderd products from woolies.</returns>
        [HttpGet("/sort")]
        public async Task<ActionResult<List<Product>>> GetSort([FromQuery] string sortOption = "")
        {
            IEnumerable<Product> products = await _wooliesClient.GetProductsAsync(_token);
            if (sortOption.ToLowerInvariant().Equals("recommended"))
            {
                

            }
            switch (sortOption.ToLowerInvariant())
            {
                case "low":
                    products = _productSortService.SortProducts(products, SortType.Low);
                    break;
                case "high":
                    products = _productSortService.SortProducts(products, SortType.High);
                    break;
                case "ascending":
                    products = _productSortService.SortProducts(products, SortType.Ascending);
                    break;
                case "descending":
                    products = _productSortService.SortProducts(products, SortType.Descending);
                    break;
                case "recommended":
                    {
                        List<ShopperHistory> shopperHistory = await _wooliesClient.GetShopperHistoryAsync(_token);
                        if (shopperHistory == null)
                        {
                            return NotFound();
                        }

                        // combine available products with all shoppers history of products so all products are listed in final output
                        IEnumerable<Product> flattenedList = shopperHistory.SelectMany(sh => sh.products).Concat(products);

                        products = _productSortService.SortProducts(flattenedList, SortType.Recommended).ToList();
                        break;
                    }
                default:
                    break;
            }
            return products.ToList();
        }

        /// <summary>
        /// Calculate the total trolley cost after applying specials.
        /// </summary>
        /// <param name="data">The trolley data containing products, quantities, and specials.</param>
        /// <returns>Total price for trolley items.</returns>
        [HttpPost("/trolleyTotal")]
        public ActionResult<decimal> GetTrolleyTotal([FromBody] trolleyData data)
        {
            List<SimpleProduct> products = data.products;
            List<Quantities> quantities = data.quantities;
            List<Specials> specials = data.specials;

            decimal maxPrice = _trolleyCalculatorService.SumProducts(products, quantities);

            decimal total = 0;
            while(specials.Any(ds => ds.quantities.All(sq => quantities.Any(dq => dq.name == sq.name && dq.quantity >= sq.quantity))))
            {
                total += _trolleyCalculatorService.ApplyOptimalSpecial(products, quantities, specials);
            }
            
            total += _trolleyCalculatorService.SumProducts(products, quantities);

            return Math.Min(maxPrice, total);
        }


    }
}
