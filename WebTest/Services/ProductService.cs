﻿using System.Collections.Generic;
using System.Linq;
using WebTest.Models;

namespace WebTest.Services
{
    public interface IProductService
    {
        /// <summary>
        /// Sort products provided based on the sort type specified.
        /// </summary>
        /// <param name="products">List of products to sort.</param>
        /// <param name="sortType">Enum representing the type of sorting to be performed.</param>
        /// <returns>List of unique ordered/sorted products.</returns>
        IEnumerable<Product> SortProducts(IEnumerable<Product> products, SortType sortType);
    }

    public class ProductService : IProductService
    {
        public IEnumerable<Product> SortProducts(IEnumerable<Product> products, SortType sortType)
        {
            IEnumerable<Product> orderedProducts = null;

            switch (sortType)
            {
                case SortType.Low:
                    orderedProducts = products.OrderBy(p => p.price);
                    break;
                case SortType.High:
                    orderedProducts = products.OrderByDescending(p => p.price);
                    break;
                case SortType.Ascending:
                    orderedProducts = products.OrderBy(p => p.name);
                    break;
                case SortType.Descending:
                    orderedProducts = products.OrderByDescending(p => p.name);
                    break;
                case SortType.Recommended:
                    {
                        var groupByProduct = products.GroupBy(fl => fl.name);
                        // popularity based on total quantity or number of orders
                        var productWithCount = groupByProduct.Select(g => new { Value = g.First(), Count = g.Select(pr => pr.quantity).Sum() });
                        orderedProducts = productWithCount.OrderByDescending(pc => pc.Count).Select(pc => pc.Value);
                        break;
                    }
                default:
                    break;
            }

            return orderedProducts;
        }
    }
}
