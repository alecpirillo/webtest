﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebTest.Models;

namespace WebTest.Services
{
    public interface ITrolleyCalculatorService
    {
        /// <summary>
        /// From the supplied specials, select and apply the single most appropriate special to maximise savings.
        /// </summary>
        /// <param name="products">List of available products and their price.</param>
        /// <param name="quantities">List of products to purchase with quantities.</param>
        /// <param name="specials">List of special pricing to apply to certain product quanitities.</param>
        /// <returns>Cost of optimal special.</returns>
        decimal ApplyOptimalSpecial(List<SimpleProduct> products, List<Quantities> quantities, List<Specials> specials);
        /// <summary>
        /// Calculates the total cost of the supplied products based on the provided quanities.
        /// </summary>
        /// <param name="products">List of available products and their price.</param>
        /// <param name="quantities">List of products to purchase with quantities.</param>
        /// <returns>Total cost of products.</returns>
        decimal SumProducts(List<SimpleProduct> products,List<Quantities> quantities);
    }

    public class TrolleyCalculatorService : ITrolleyCalculatorService
    {
        public decimal ApplyOptimalSpecial(List<SimpleProduct> products, List<Quantities> quantities, List<Specials> specials)
        {
            // order by savings amount to prioritize best specials
            var validSpecials = specials.Where(ds => ds.total > 0 && ds.quantities.All(sq => quantities.Any(dq => dq.name == sq.name && dq.quantity >= sq.quantity)));
            var specialCosts = validSpecials.Select(ds => {
                var productsDone = ds.quantities.Select(g => new { Value = g, Cost = g.quantity * products.FirstOrDefault(t => t.name == g.name).price, remaining = g.quantity == 0 ? 0 : quantities.FirstOrDefault(t => t.name == g.name).quantity / g.quantity });
                return new
                {
                    Value = ds,
                    // Balancing between cost savings for this single special vs remaining possible usages of this special is important. A special with a high cost saving shold not be prevent many specials with a lower but higher combined saving from applying.
                    savings = (productsDone.Sum(t => t.Cost) - ds.total) / productsDone.Sum(t => t.Cost),
                    remaining = productsDone.Sum(t => t.remaining)
                };
            });
            Specials bestSpecial = specialCosts.OrderByDescending(sc => sc.remaining).ThenByDescending(t => t.savings).Select(sc => sc.Value).First();

            decimal maxProductPrice = RemoveQuantitiesAndCalculateMaxPrice(bestSpecial, products, quantities);

            // the special cannot cost more then the sum of the products - return the lowest of the two;
            return Math.Min(bestSpecial.total, maxProductPrice);
        }

        /// <summary>
        /// Remove quantities for products in bestSpecial and calculate the total price of the specia.
        /// </summary>
        /// <param name="bestSpecial">Most appropriate special to apply to the trolley.</param>
        /// <param name="products">List of products with prices.</param>
        /// <param name="quantities">List of products to purchase with quantities.</param>
        /// <returns></returns>
        private decimal RemoveQuantitiesAndCalculateMaxPrice(Specials bestSpecial, List<SimpleProduct> products, List<Quantities> quantities)
        {
            decimal maxProductPrice = 0;
            foreach (var product in quantities)
            {
                var specialProduct = bestSpecial.quantities.FirstOrDefault(sq => sq.name == product.name);
                if (specialProduct != null)
                {
                    product.quantity -= specialProduct.quantity;
                    var productPrice = products.FirstOrDefault(t => t.name == product.name);
                    if (productPrice != null)
                    {
                        maxProductPrice += productPrice.price * specialProduct.quantity;
                    }
                }
            }
            return maxProductPrice;
        }

        public decimal SumProducts(List<SimpleProduct> products, List<Quantities> quantities)
        {
            decimal total = 0;
            foreach (var product in quantities)
            {
                var productPrice = products.FirstOrDefault(p => p.name == product.name);
                if (productPrice != null)
                {
                    total += product.quantity * productPrice.price;
                }
            }

            return total;
        }
    }
}
