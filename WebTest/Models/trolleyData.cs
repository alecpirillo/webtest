﻿using System.Collections.Generic;

namespace WebTest.Models
{
    public class trolleyData
    {
        public List<SimpleProduct> products { get; set; }
        public List<Specials> specials { get; set; }
        public List<Quantities> quantities { get; set; }

    }

    public class SimpleProduct
    {
        public string name { get; set; }
        public decimal price { get; set; }
    }

    public class Specials
    {
        public decimal total { get; set; }
        public List<Quantities> quantities { get; set; }
    }

    public class Quantities
    {
        public string name { get; set; }
        public int quantity { get; set; }
    }
}
