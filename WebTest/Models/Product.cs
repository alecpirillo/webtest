﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTest.Models
{
    public class Product
    {
        public string name { get; set; }
        public decimal price { get; set; }
        public double quantity { get; set; }
    }
}
