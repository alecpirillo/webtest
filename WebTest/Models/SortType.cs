﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTest.Models
{
    public enum SortType
    {
        /// <summary>
        /// Lowest to highest price
        /// </summary>
        Low,
        /// <summary>
        /// Highest to lowest price
        /// </summary>
        High,
        /// <summary>
        /// Name A to Z
        /// </summary>
        Ascending,
        /// <summary>
        /// Name Z to A
        /// </summary>
        Descending,
        /// <summary>
        /// Order by highest occurence
        /// </summary>
        Recommended
    }
}
