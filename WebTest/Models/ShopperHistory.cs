﻿using System.Collections.Generic;

namespace WebTest.Models
{
    public class ShopperHistory
    {
        public int customerId { get; set; }
        public List<Product> products { get; set; }
    }
}
