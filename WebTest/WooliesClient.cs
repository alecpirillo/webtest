﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using WebTest.Models;

namespace WebTest
{
    public class WooliesClient : HttpClient
    {
        public WooliesClient(string url)
        {
            BaseAddress = new Uri(url + "api/resource/");
            DefaultRequestHeaders.Accept.Clear();
            DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<IEnumerable<Product>> GetProductsAsync(string token)
        {
            List<Product> products = null;
            HttpResponseMessage response = await GetAsync("products?token=" + token);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync();
                products = JsonConvert.DeserializeObject<List<Product>>(result);
            }
            return products;
        }

        public async Task<List<ShopperHistory>> GetShopperHistoryAsync(string token)
        {
            List<ShopperHistory> shopperHistory = null;
            HttpResponseMessage response = await GetAsync("shopperHistory?token=" + token);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync();
                shopperHistory = JsonConvert.DeserializeObject<List<ShopperHistory>>(result);
            }
            return shopperHistory;
        }

        public async Task<decimal> CalculateTrolleyAsync( trolleyData data, string token)
        {
            decimal total = 0;
            var myContent = JsonConvert.SerializeObject(data);
            var stringContent = new StringContent(myContent, UnicodeEncoding.UTF8, "application/json");
            HttpResponseMessage response = await PostAsync("trolleyCalculator?token=" + token, stringContent);
            if (response.IsSuccessStatusCode)
            {
                total = await response.Content.ReadAsAsync<decimal>();
            }
            return total;
        }
    }
}
