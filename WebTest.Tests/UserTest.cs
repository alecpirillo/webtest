using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using WebTest.Controllers;
using WebTest.Services;
using Xunit;

namespace WebTest.Tests
{
    public class UserTest
    {
        private readonly AnswersController _answersController;
        private readonly IConfiguration _configuration;
        private readonly ITrolleyCalculatorService _trolleyCalculatorService;
        private readonly IProductService _productSortService;
        private readonly string token = "6eb70e3c-3e68-4ccc-8abb-f0bd5658e8d5";
        private readonly string name = "Alec Pirillo";

        public UserTest()
        {
            var mockConfig = new Mock<IConfiguration>();
            mockConfig.SetupGet(p => p["Token"]).Returns(token);
            mockConfig.SetupGet(p => p["WooliesApi"]).Returns("http://dev-wooliesx-recruitment.azurewebsites.net/");
            mockConfig.SetupGet(p => p["Name"]).Returns(name);
            _configuration = mockConfig.Object;

            var mockTrolleyCalcService = new Mock<ITrolleyCalculatorService>();
            _trolleyCalculatorService = mockTrolleyCalcService.Object;

            var mockProductService = new Mock<IProductService>();
            _productSortService = mockProductService.Object;

            _answersController = new AnswersController(_configuration, _trolleyCalculatorService, _productSortService);
        }

        [Fact]
        public void UserNameAndTokenAreCorrect()
        {
            // arrange

            // act
            var actionResult = _answersController.GetUser();

            //assert
            Assert.Equal(actionResult.Value.Name, name);
            Assert.Equal(actionResult.Value.Token, token);
        }
    }
}
